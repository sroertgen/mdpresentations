# Orange Workflows

## twitter_polizei_workflow.ows

Dies ist der [Orange3](https:orange.biolab.si)-Workflow für den Workshop "Twitter-Polizei".

**Voraussetzungen**:
  - Orange3 runterladen
  - das Add-On "Text" installieren (unter "Options / Add-ons...")
  - Workflow laden
