---
author: Steffen Rörtgen
title: Lighning Talk Sommer- und IT-Camp 2019
date: 26.08.2019
theme: simple
slideNumber: true
---

# KI für OER

https://kurzelinks.de/ki-fuer-oer

<section data-background-iframe="https://kurzelinks.de/ki-fuer-oer" data-background-interactive></section>

##

## KI-Workshop

:::::::::::::: {.columns}
::: {.column width="60%"}

- **X5GON** ist hier
- zwei niederländische Projekte:
  - **SURF**: Jelmer de Ronde
  - **TU Delft**: Michiel de Jong

- **Auffindbarkeit von OER**
  - Einbindung von User-Informationen und OER-Qualitätskriterien

- besteht Interesse an einem Austausch?
:::
::: {.column width="40%"}

![](../images/qr-code-kurzelinks-de-ki-fuer-oer.png){ width=100% }

:::
::::::::::::::

# Uni <br> Schule <br> OER

## Wie kommen Hochschule und Schule im Bereich OER zusammen?

> - Beide arbeiten an verschiedenen Stellen an OER-Projekten
> - **aber** sie arbeiten nicht zusammen

## Wie kann ein Austausch initiiert werden?

> - Use-Cases für eine mögliche Zusammenarbeit sammeln (Lehrer:innen-Bildung)
> - Austausch auf Infrastruktur-Ebene (DFN, RfII)

# Vielen Dank!